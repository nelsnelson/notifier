#! /bin/sh

twiliorc="${TWILIORC:-~/.twiliorc}"
message="${@:-Notification from ${HOSTNAME}}"

if [ ! -f "${twiliorc}" ]; then
    echo "A $HOME/.twiliorc file is required"
    exit 1
fi
source "${twiliorc}"

url="https://api.twilio.com/2010-04-01/Accounts/${twilio_sid}/Messages.json"
curl --request POST "${url}" \
  --data-urlencode "Body=${message}" \
  --data-urlencode "From=${twilio_number}" \
  --data-urlencode "To=${your_telephone_number}" \
  --header "Accept: application/json" \
  --header "Content-Type: application/json" \
  --user "${twilio_sid}:${auth_token}"

