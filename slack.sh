#! /bin/sh

message="${1:-"Notice from $(hostname)!"}"

if [ -f $HOME/.slackrc ]; then
    source $HOME/.slackrc
fi
[[ -z $token ]] && die "Requirement not satisfied: ${$HOME}/.slackrc must contain: token=slack_app_api_token"

# results=$(curl --silent --location --request GET 'https://slack.com/api/conversations.list' --header "Authorization: Bearer ${token}" --header 'Accept: application/json' --data '{"types": "mpim"}')

direct_message_channel_id=$(curl --silent --location --request POST 'https://slack.com/api/conversations.open' --header "Authorization: Bearer ${token}" --header 'Content-type: application/json' --header 'Accept: application/json' --data "{\"users\": \"${user_id}\"}" | jq --raw-output '.channel.id')

results=$(curl --silent --location --request POST 'https://slack.com/api/chat.postMessage' --header "Authorization: Bearer ${token}" --header 'Content-type: application/json; charset=utf-8' --header 'Accept: application/json' --data "{\"channel\": \"${direct_message_channel_id}\", \"text\": \"${message}\"}" | jq --raw-output '.message.text')

echo "Sent notification: ${results}"
