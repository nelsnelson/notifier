#! /bin/sh

monitor_subject="test.sh"
notifier="slack.sh"
script_dir_path="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
cmd="${script_dir_path}/${monitor_subject}"
notifier_cmd="${script_dir_path}/${notifier}"
message="Your thing is finished at ${HOSTNAME}, ${USER}."
always_succeed="${ALWAYS_SUCCEED:-0}"

while true; do
  echo "Executing command: ${cmd}"
  result=$(sh "${cmd}")
  echo "result: ${result}"

  if [[ -z "${result}" && $always_succeed -eq 0 ]]; then 
    echo 'Monitor did not meet criteria; sleeping 10 seconds'
    sleep 10
  else
    break
  fi
done

sh "${notifier_cmd}" "${message}"

